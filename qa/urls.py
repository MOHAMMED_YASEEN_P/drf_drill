from django.urls import path
from .views import GenericAPIView, GenericQuestionDetails, GenericAnswerDetails

app_name = 'qa'
urlpatterns = [
    path('',GenericAPIView.as_view()),
    path('/<int:pk>',GenericQuestionDetails.as_view(), name="question-detail"),
    path('/<int:pk>/answers',GenericAnswerDetails.as_view()),
]
